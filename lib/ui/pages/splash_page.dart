import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:once_app/utils/constants.dart';

import 'home_page.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(Duration(seconds: 5), () {
      Navigator.pushNamedAndRemoveUntil(
          context, HomePage.tag, (Route<dynamic> route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(background),
      body: Column(
        children: [
          Expanded(
              child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Text(
                  //   'Welcome',
                  //   style: TextStyle(fontSize: 24, fontWeight: FontWeight.w800),
                  // ),
                  SizedBox(
                    height: 15,
                  ),
                  Stack(
                    children: [
                      Text(
                        "Once",
                        style: GoogleFonts.seaweedScript(
                          textStyle: TextStyle(
                            color: fromHex(green),
                            fontSize: 56,
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 0,
                          right: 0,
                          child: Text(
                            'app',
                            style: TextStyle(
                              color: fromHex(white),
                            ),
                          ))
                    ],
                  )
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
