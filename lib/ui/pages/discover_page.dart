import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:once_app/utils/constants.dart';

class Discover extends StatefulWidget {
  static const tag = 'discover';
  @override
  _DiscoverState createState() => _DiscoverState();
}

class _DiscoverState extends State<Discover> {
  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: fromHex(black),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      body: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          _topbar(context),
          _searchBar(),
          Expanded(child: _tabs(context))
        ],
      ),
    );
  }

  Widget _searchBar() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Container(
        width: double.infinity,
        height: 40,
        decoration: BoxDecoration(
          color: fromHex(primary_light).withOpacity(0.9),
          borderRadius: BorderRadius.circular(50.0),
        ),
        child: TextField(
          cursorColor: fromHex(primary_light),
          decoration: InputDecoration(
            prefixIcon: Icon(
              Icons.search,
              color: Colors.grey.withOpacity(0.5),
            ),
            hintText: "Search",
            hintStyle: TextStyle(
              height: 1.7,
              color: Colors.grey.withOpacity(0.8),
            ),
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }

  Widget _topbar(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Discover",
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 32.0),
          ),
          Container(
            child: CircleAvatar(
              radius: 30,
              backgroundImage: AssetImage(
                "assets/fab1.png",
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _tabs(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            color: Colors.black,
            child: TabBar(
              isScrollable: true,
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("All"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Arts"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Music"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Interior Design"),
                  ),
                ),
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("News"),
                  ),
                )
              ],
              unselectedLabelColor: Colors.white,
              labelColor: fromHex(white),
              indicatorColor: Colors.transparent,
              indicator: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          topRight: Radius.circular(50),
                          bottomLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50))),
                  color: Colors.green),
            ),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                _grid(context),
                _grid(context),
                _grid(context),
                _grid(context),
                _grid(context),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _grid(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150.0,
        childAspectRatio: 0.5,
        crossAxisSpacing: 2.0,
        mainAxisSpacing: 5.0,
      ),
      itemBuilder: (_, index) => Padding(
        padding: const EdgeInsets.all(3.0),
        child: CachedNetworkImage(
          imageUrl: image,
          fit: BoxFit.cover,
        ),
      ),
      itemCount: 100,
    );
  }
}
