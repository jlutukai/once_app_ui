import 'package:flutter/material.dart';
import 'package:once_app/ui/pages/wallet_page.dart';
import 'package:once_app/utils/constants.dart';
import 'package:path/path.dart';

class QuestionPage extends StatefulWidget {
  static const tag = 'question';
  @override
  _QuestionPageState createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(black),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 50,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(context, WalletPage.tag,
                          (Route<dynamic> route) => false);
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    )),
                Expanded(child: Text("")),
              ],
            ),
            _settingTitle(),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10.0, bottom: 5.0, left: 20, right: 22.0),
              child: Container(
                width: double.infinity,
                height: 40,
                decoration: BoxDecoration(
                  color: fromHex(primary_light).withOpacity(0.9),
                  borderRadius: BorderRadius.circular(50.0),
                ),
                child: TextField(
                  cursorColor: fromHex(primary_light),
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.search,
                      color: Colors.grey.withOpacity(0.5),
                    ),
                    hintText: "Search",
                    hintStyle: TextStyle(
                      height: 1.7,
                      color: Colors.grey.withOpacity(0.8),
                    ),
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 8.0,
                left: 20,
                bottom: 8.0,
              ),
              child: Text(
                "Check it out in our list of FAQs below",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(
                    top: 5.0, left: 20.0, right: 20.0, bottom: 10.0),
                child: _settingList(context),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    child: Row(
                      children: [
                        Text(
                          'load more',
                          style: TextStyle(
                            color: Colors.greenAccent,
                            fontSize: 16.0,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                        Icon(
                          Icons.arrow_right_alt,
                          color: Colors.greenAccent,
                        )
                      ],
                    ),
                    onTap: () => {},
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 1.0),
                    child: Text("Can not find your question?",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10),
                    child: Text(
                      "Contact our support team!",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.87,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                    shape: BoxShape.rectangle,
                    image: DecorationImage(
                        image: NetworkImage(image), fit: BoxFit.cover),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0, top: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ElevatedButton(
                          onPressed: () {},
                          child: Text(
                            "Contact Us",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Row _settingTitle() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 20),
          child: Text(
            "Have a question?",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 32,
            ),
          ),
        ),
      ],
    );
  }

  _settingList(BuildContext context) {
    final List<String> settings = <String>[
      "Cannot find a transction in the list",
      "How could i cancel a bid?",
      "Cannot proceed the funds transfer",
      "How can i return the money from transaction?"
    ];

    return ListView.builder(
      itemBuilder: (ctx, int index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Container(
            height: 48.0,
            decoration: BoxDecoration(
              color: fromHex(primary_light),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(
                        "${index + 1}. ",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(settings[index],
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.6),
                              fontSize: 14.0,
                            ),
                            maxLines: 2,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
      itemCount: settings.length,
    );
  }
}
