import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:once_app/ui/pages/single_chat_page.dart';
import 'package:once_app/utils/constants.dart';

class ChatPage extends StatefulWidget {
  static const tag = 'chat_page';
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: fromHex(background),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            SizedBox(
              height: 56,
            ),
            _topbar(context),
            _searchbar(context),
            _roomsContent(context),
            Container(
                height: MediaQuery.of(context).size.height*0.5,
                child: _chatArea(context)),
          ],
        ),
      ),
    );
  }

  Widget _roomsContent(context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Row(
            children: [
              Text(
                "ROOMS",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        _rooms(context)
      ],
    );
  }

  Widget _chatArea(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
          child: Row(
            children: [
              Text(
                "PERSONAL CHATS",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        Expanded(child: _conversations(context)),
      ],
    );
  }

  Widget _rooms(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: fromHex(primary_light),
                  border: Border.all(color: Colors.green),
                ),
                child: Center(
                  child: Icon(
                    Icons.group_add_rounded,
                    color: Colors.green,
                    size: 33,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                width: 75,
                child: Align(
                    child: Text(
                  '+ Add New',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.green),
                )),
              )
            ],
          ),

          Expanded(
            child: ListView.builder(
                itemCount: rooms.length,
                scrollDirection: Axis.horizontal,
                itemBuilder:(context, index)=> Column(
              children: <Widget>[
                Container(
                  width: 60,
                  height: 60,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: fromHex(primary_light), width: 1)),
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Container(
                            width: 75,
                            height: 75,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(image),
                                    fit: BoxFit.cover)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  width: 75,
                  child: Align(
                      child: Text(
                        rooms[index]['name'],
                        style: TextStyle(color: Colors.white),
                        overflow: TextOverflow.ellipsis,
                      )),
                )
              ],
            ) ),
          )

        ],
      ),
    );
  }

  Widget _searchbar(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(
              "Inbox (4)",
              style: TextStyle(
                color: Colors.white,
                fontSize: 32.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Text(
                "Notification (15)",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14.0,
                  fontWeight: FontWeight.w400,
                ),
              ),
            )
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
              color: fromHex(primary_light).withOpacity(0.9),
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: TextField(
              cursorColor: fromHex(primary_light),
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.grey.withOpacity(0.5),
                ),
                hintText: "search",
                hintStyle: TextStyle(
                  height: 1.7,
                  color: Colors.grey.withOpacity(0.8),
                ),
                border: InputBorder.none,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _topbar(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          child: CircleAvatar(
            radius: 30,
            backgroundImage: AssetImage(
              "assets/fab1.png",
            ),
          ),
        )
      ],
    );
  }

  _conversations(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: conversationList.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: (){
            Navigator.of(context).pushNamed(SingleChatPage.tag);
          },
          child: Padding(
            padding: const EdgeInsets.only(bottom: 15.0),
            child: Container(
              // color: Colors.red,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(image),
                                fit: BoxFit.cover)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 23.0, left: 10.0, bottom: 7.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 7.0),
                              child: Text(
                                conversationList[index]["name"],
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17),
                              ),
                            ),
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(
                                    conversationList[index]
                                        ["message"],
                                    style: TextStyle(
                                      color: Colors.white
                                          .withOpacity(0.6),
                                      fontSize: 14.0,
                                    ),
                                    maxLines: 2,
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 23.0,
                      left: 23.0,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            conversationList[index]["time"],
                            style: GoogleFonts.rajdhani(
                              textStyle: TextStyle(
                                color: Colors.white.withOpacity(0.5),
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Container(
                            width: 15,
                            height: 15,
                            decoration: BoxDecoration(
                              color: Colors.greenAccent,
                              borderRadius: BorderRadius.all(
                                Radius.circular(20.0),
                              ),
                            ),
                            child: Center(child: Text("2")),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
