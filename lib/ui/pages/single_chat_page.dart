import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:once_app/utils/constants.dart';

class SingleChatPage extends StatefulWidget {
  static const tag = 'single_chat_page';
  const SingleChatPage({Key? key}) : super(key: key);

  @override
  _SingleChatPageState createState() => _SingleChatPageState();
}

class _SingleChatPageState extends State<SingleChatPage> {
  late double height, width;
  TextEditingController messageController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: fromHex(background),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Stack(
                children: [
                  Container(
                    height: 129,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: fromHex(background),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "Chats",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                              fontSize: 25),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 20,
                    top: 35,
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.keyboard_backspace_sharp,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
              Expanded(
                child: Container(),
                // child: StreamBuilder<QuerySnapshot>(
                //   stream: _firestore
                //       .collection("messages")
                //       .doc(widget.id)
                //       .collection(widget.type)
                //       .orderBy('date', descending: true)
                //       .snapshots(),
                //   builder: (context, snapshot) {
                //     if (!snapshot.hasData)
                //       return Center(
                //         child: CircularProgressIndicator(
                //           backgroundColor: fromHex(gold_dark),
                //           valueColor: new AlwaysStoppedAnimation<Color>(
                //             fromHex(grey),
                //           ),
                //         ),
                //       );
                //
                //     List<QueryDocumentSnapshot> docs = snapshot.data.docs;
                //
                //     List<Widget> messages = docs
                //         .map((doc) => doc['from_id'] == getCurrentUser().id
                //         ? MyMessageWidget(
                //       content: doc['message'],
                //       time: doc['date'],
                //       name: doc['name'],
                //       isSame: false,
                //       hasHeading: true,
                //     )
                //         : RecievedMessageWidget(
                //       content: doc['message'],
                //       time: doc['date'],
                //       name: doc['name'],
                //       isSame: false,
                //       hasHeading: true,
                //     ))
                //         .toList();
                //
                //     return ListView(
                //       reverse: true,
                //       controller: scrollController,
                //       children: <Widget>[
                //         ...messages,
                //       ],
                //     );
                //   },
                // ),
              ),
              Container(
                child: buildInputArea(),
              ),
            ],
          ),
        ),
      );
  }

  // Future<void> callback() async {
  //   if (messageController.text.length > 0) {
  //     await _firestore
  //         .collection("messages")
  //         .doc(widget.id)
  //         .collection(widget.type)
  //         .add({
  //       'message': messageController.text,
  //       'from_id': getCurrentUser().id,
  //       'name': getCurrentUser().name.first,
  //       'date': DateTime.now().toIso8601String().toString(),
  //     });
  //     messageController.clear();
  //     scrollController.animateTo(
  //       scrollController.position.minScrollExtent,
  //       curve: Curves.easeOut,
  //       duration: const Duration(milliseconds: 300),
  //     );
  //   }
  // }

  buildInputArea() {
    return Container(
      height: height * 0.1,
      width: width,
      child: Row(
        children: <Widget>[
          buildChatInput(),
          Expanded(
            child: buildSendButton()

          ),
        ],
      ),
    );
  }

  Widget buildChatInput() {
    return Container(
      width: width * 0.7,
      padding: const EdgeInsets.all(2.0),
      margin: const EdgeInsets.only(left: 40.0),
      child: TextField(
        decoration: InputDecoration.collapsed(
            hintText: 'Send a message...',
            hintStyle: TextStyle(color: Colors.white)),
        controller: messageController,
      ),
    );
  }

  Widget buildSendButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(

          onPressed: () async {
            bool hasConnection = await checkConnection();
            if (!hasConnection) {
              showToast("Check Internet Connection");
              return;
            }

          },
          icon: Icon(
            Icons.send,
            color: fromHex(green),
          ),
        ),
      ],
    );
  }


}

class SentMessages extends StatelessWidget {
  final String content;
  final String time;
  final String name;
  final bool isSame;
  final bool hasHeading;

  const SentMessages(
      {Key? key,
        required this.content,
        required this.time,
        required this.name,
        required this.isSame,
        required this.hasHeading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print()
    return Bubble(
        margin: BubbleEdges.all(0),
        alignment: Alignment.topRight,
        nip: BubbleNip.no,
        color: Colors.transparent,
        padding: BubbleEdges.all(0),
        elevation: 0,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Bubble(
              margin: BubbleEdges.only(top: 10),
              alignment: Alignment.topRight,
              nip: !isSame ? BubbleNip.rightBottom : BubbleNip.no,
              color: Colors.blue.shade50,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    Jiffy(DateTime.tryParse(time)).format("HH:mm"),
                    style: TextStyle(color: Colors.grey, fontSize: 11),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        "you",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            color: fromHex(blue),
                            fontSize: 12),
                      ),
                      Container(
                        constraints: BoxConstraints(
                            minWidth: 80,
                            maxWidth: MediaQuery.of(context).size.width - 200),
                        child: Bubble(
                          color: Colors.transparent,
                          elevation: 0,
                          padding: BubbleEdges.all(0),
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.black),
                              children: <TextSpan>[
                                TextSpan(text: "${content}"),
//                                          TextSpan(
//                                              text: '@marylyn',style: TextStyle(color: Colors.blue,)
//                                          ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            !isSame
                ? CircleAvatar(
              radius: 15.0,
              backgroundColor: fromHex(blue),
              child: Center(
                child: Text(
                  name.substring(0, 1),
                  style: TextStyle(
                    color: fromHex(green),
                  ),
                ),
              ),
            )
                : Container(
              width: 24,
            ),
            SizedBox(
              width: 5,
            ),
          ],
        ));
  }
}

class ReceivedMessage extends StatelessWidget {
  final String content;
  final String time;
  final String name;
  final bool isSame;
  final bool hasHeading;

  const ReceivedMessage(
      {Key? key,
        required this.content,
        required this.time,
        required this.name,
        required this.isSame,
        required this.hasHeading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Bubble(
      margin: BubbleEdges.all(0),
      alignment: Alignment.topLeft,
      nip: BubbleNip.no,
      color: Colors.transparent,
      padding: BubbleEdges.all(0),
      elevation: 0,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          SizedBox(
            width: 5,
          ),
          !isSame
              ? CircleAvatar(
            radius: 15.0,
            backgroundColor: fromHex(blue),
            child: Center(
              child: Text(
                name.substring(0, 1),
                style: TextStyle(color: fromHex(grey)),
              ),
            ),
          )
              : Container(
            width: 24,
          ),
          SizedBox(
            width: 10,
          ),
          Bubble(
            margin: BubbleEdges.only(top: 10),
            alignment: Alignment.topLeft,
            nip: !isSame ? BubbleNip.leftBottom : BubbleNip.no,
            color: Colors.teal.shade50,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "$name",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontStyle: FontStyle.italic,
                          color: fromHex(blue),
                          fontSize: 12),
                    ),
                    Container(
                        constraints: BoxConstraints(
                            minWidth: 80,
                            maxWidth: MediaQuery.of(context).size.width - 200),
                        child: Bubble(
                          color: Colors.transparent,
                          elevation: 0,
                          padding: BubbleEdges.all(0),
                          child: RichText(
                            text: TextSpan(
                              style: TextStyle(
                                  fontSize: 14.0, color: Colors.black),
                              children: <TextSpan>[
                                TextSpan(text: "$content"),
//                                          TextSpan(
//                                              text: '@marylyn',style: TextStyle(color: Colors.blue,)
//                                          ),
                              ],
                            ),
                          ),
                        )),
                  ],
                ),
                Text(
                  Jiffy(DateTime.tryParse(time)).format("HH:mm"),
                  style: TextStyle(color: Colors.grey, fontSize: 11),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
