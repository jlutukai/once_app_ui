import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:once_app/ui/pages/profile_page.dart';
import 'package:once_app/ui/pages/question_page.dart';
import 'package:once_app/ui/pages/settings_page.dart';
import 'package:once_app/ui/pages/take_moment.dart';
import 'package:once_app/ui/pages/wallet_settings_page.dart';
import 'package:once_app/utils/constants.dart';

class WalletPage extends StatefulWidget {
  static const tag = 'wallet';
  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(black),
      body: Column(
        children: [
          SizedBox(
            height: 60,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(context,
                          ProfilePage.tag, (Route<dynamic> route) => true);
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    )),
                Container(
                  child: CircleAvatar(
                    radius: 30,
                    backgroundImage: AssetImage(
                      "assets/fab1.png",
                    ),
                  ),
                )
              ],
            ),
          ),
          _banner(context),
          Expanded(child: _tabs(context))
        ],
      ),
    );
  }

  _banner(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        padding: EdgeInsets.only(top: 3.0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.25,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Wallet",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 34.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: IconButton(
                          icon: Icon(Icons.settings),
                          color: Colors.white,
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(
                                context,
                                WalletSetting.tag,
                                (Route<dynamic> route) => true);
                          },
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: IconButton(
                      icon: Icon(Icons.help_outlined),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.pushNamedAndRemoveUntil(context,
                            QuestionPage.tag, (Route<dynamic> route) => true);
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.18,
              decoration: BoxDecoration(
                  color: fromHex(background),
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 18.0, bottom: 5.0, right: 18.0, left: 18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "ACTIVE BALANCE:",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15.0,
                              fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0, right: 18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          // crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              "\$ ",
                              style: GoogleFonts.londrinaOutline(
                                textStyle: TextStyle(
                                  color: fromHex(green),
                                  fontSize: 30,
                                ),
                              ),
                            ),
                            Text(
                              "950.00",
                              style: GoogleFonts.londrinaOutline(
                                textStyle: TextStyle(
                                  color: fromHex(green),
                                  fontSize: 56,
                                ),
                              ),
                            ),
                            // Text(
                            //   " .00",
                            //   style: GoogleFonts.londrinaOutline(
                            //     textStyle: TextStyle(
                            //       color: fromHex(green),
                            //       fontSize: 20,
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 18.0, right: 18.0, bottom: 4.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "LOCKED IN BIDS:",
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "PENDING:",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 18.0, right: 18.0, bottom: 9.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "\$",
                              style: GoogleFonts.oxygen(
                                textStyle: TextStyle(
                                  color: Colors.yellow,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                            Text(
                              "299.99",
                              style: GoogleFonts.oxygen(
                                textStyle: TextStyle(
                                  color: Colors.yellow,
                                  fontSize: 25,
                                ),
                              ),
                            ),
                            // Text(
                            //   ".99",
                            //   style: GoogleFonts.oxygen(
                            //     textStyle: TextStyle(
                            //       color: Colors.yellow,
                            //       fontSize: 10,
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "\$",
                              style: GoogleFonts.oxygen(
                                textStyle: TextStyle(
                                  color: Colors.yellow,
                                  fontSize: 20,
                                ),
                              ),
                            ),
                            Text(
                              "12.55",
                              style: GoogleFonts.oxygen(
                                textStyle: TextStyle(
                                  color: Colors.yellow,
                                  fontSize: 25,
                                ),
                              ),
                            ),
                            // Text(
                            //   ".55",
                            //   style: GoogleFonts.oxygen(
                            //     textStyle: TextStyle(
                            //       color: Colors.yellow,
                            //       fontSize: 10,
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _tabs(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            color: Colors.black,
            child: TabBar(
              tabs: [
                Tab(
                  text: "History",
                ),
                Tab(
                  text: "Images",
                ),
                Tab(
                  text: "Rewards",
                ),
                Tab(
                  text: "Coins",
                ),
              ],
              unselectedLabelColor: Colors.white,
              labelColor: fromHex(green),
              indicatorColor: Colors.transparent,
            ),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                _transactionList(context),
                _imagesGrid(context),
                _rewardsList(context),
                _coinsList(context),
              ],
            ),
          )
        ],
      ),
    );
  }

  _imagesGrid(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150.0,
        childAspectRatio: 0.5,
        crossAxisSpacing: 2.0,
        mainAxisSpacing: 5.0,
      ),
      itemBuilder: (_, index) => Padding(
        padding: const EdgeInsets.all(3.0),
        child: CachedNetworkImage(
          imageUrl: image,
          fit: BoxFit.cover,
        ),
      ),
      itemCount: 100,
    );
  }

  Widget _transactionList(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      padding: EdgeInsets.only(top: 15.0, bottom: 8.0, left: 15.0, right: 15.0),
      decoration: BoxDecoration(
        color: fromHex(background),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(
                Icons.signal_cellular_alt_outlined,
                color: Colors.white.withOpacity(0.6),
                size: 22.0,
              ),
              Text(
                "JUN 26, 2021",
                style: TextStyle(
                    color: Colors.white.withOpacity(0.6), fontSize: 22.0),
              ),
              Icon(
                Icons.search_outlined,
                color: Colors.white.withOpacity(0.6),
                size: 22.0,
              )
            ],
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 8.0, right: 8.0),
              child: ListView.separated(
                itemBuilder: (context, int index) {
                  return Container(
                    padding: EdgeInsets.only(bottom: 1.0),
                    height: 80.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: NetworkImage(image),
                                      fit: BoxFit.cover)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 23.0, left: 10.0, bottom: 7.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 7.0),
                                    child: Text(
                                      transactionsSummary[index]["title"],
                                      style: TextStyle(
                                        color: Colors.white38,
                                      ),
                                    ),
                                  ),
                                  DecoratedBox(
                                    decoration: BoxDecoration(
                                      color: getTransctionSummyBgColor(
                                          transactionsSummary[index]["type"]),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(6.0),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Text(
                                        transactionsSummary[index]["type"],
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 23.0,
                            left: 23.0,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                transactionsSummary[index]["price"],
                                style: GoogleFonts.oxygen(
                                  textStyle: TextStyle(
                                    color: fromHex(
                                        transactionsSummary[index]["color"]),
                                    fontSize: 25,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
                itemCount: transactionsSummary.length,
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(
                  color: Colors.white10,
                  height: 1.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _coinsList(BuildContext context) {
    final List<String> rewards = <String>[
      "USDT",
      "USDC",
    ];
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      decoration: BoxDecoration(
        color: fromHex(background),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
            top: 20.0, left: 15.0, right: 15.0, bottom: 15.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  Icons.signal_cellular_alt_outlined,
                  color: Colors.white.withOpacity(0.6),
                  size: 22.0,
                ),
                Text(
                  "MY COINS",
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.6), fontSize: 22.0),
                ),
                Icon(
                  Icons.search_outlined,
                  color: Colors.white.withOpacity(0.6),
                  size: 22.0,
                )
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, top: 10, bottom: 10.0),
                child: ListView.separated(
                  itemBuilder: (ctx, int index) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          rewards[index],
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                        index == 1
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "\$2,000.00 ",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17.0),
                                  ),
                                ],
                              )
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "\$3,000.00 ",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17.0),
                                  ),
                                ],
                              )
                      ],
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                    color: Colors.white10,
                    height: 20.0,
                  ),
                  itemCount: rewards.length,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _rewardsList(BuildContext context) {
    final List<String> rewards = <String>[
      "Once Moments",
      "Adverisement",
    ];
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      decoration: BoxDecoration(
        color: fromHex(background),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
            top: 20.0, left: 15.0, right: 15.0, bottom: 15.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  Icons.signal_cellular_alt_outlined,
                  color: Colors.white.withOpacity(0.6),
                  size: 22.0,
                ),
                Text(
                  "MY REWARDS",
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.6), fontSize: 22.0),
                ),
                Icon(
                  Icons.search_outlined,
                  color: Colors.white.withOpacity(0.6),
                  size: 22.0,
                )
              ],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 10, right: 10, top: 10, bottom: 10.0),
                child: ListView.separated(
                  itemBuilder: (ctx, int index) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          rewards[index],
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                        index == 1
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "\$12,000.00 ",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17.0),
                                  ),
                                  Icon(
                                    Icons.arrow_right_alt,
                                    color: Colors.white,
                                  )
                                ],
                              )
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "\$1,000.00 ",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 17.0),
                                  ),
                                  Icon(
                                    Icons.arrow_right_alt,
                                    color: Colors.white,
                                  )
                                ],
                              )
                      ],
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                    color: Colors.white10,
                    height: 20.0,
                  ),
                  itemCount: rewards.length,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
