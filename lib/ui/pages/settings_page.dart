import 'package:flutter/material.dart';
import 'package:once_app/ui/pages/home_page.dart';
import 'package:once_app/ui/pages/profile_page.dart';
import 'package:once_app/utils/constants.dart';

class Setting extends StatefulWidget {
  static const tag = 'settings';
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: fromHex(black),
      body: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(context, ProfilePage.tag,
                        (Route<dynamic> route) => false);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  )),
              Expanded(child: Text("")),
            ],
          ),
          _settingTitle(),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
              child: _settingList(context),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.transparent)),
                    child: Text(
                      "LOG OUT",
                      style: TextStyle(
                        color: Colors.red.withOpacity(0.3),
                        fontSize: 24,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(context, HomePage.tag,
                          (Route<dynamic> route) => true);
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Row _settingTitle() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            top: 20.0,
            bottom: 15.0,
            left: 20.0,
          ),
          child: Text(
            "General Settings",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 32,
            ),
          ),
        ),
      ],
    );
  }

  _settingList(BuildContext context) {
    final List<String> settings = <String>[
      "Profile Settings",
      "Data Storage",
      "Notification",
      "Privacy",
      "Security",
      "Help"
    ];

    return ListView.separated(
      itemBuilder: (ctx, int index) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              settings[index],
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0),
            ),
            index == 1
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "0.00 MB",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0),
                      ),
                      Icon(
                        Icons.arrow_right_alt,
                        color: Colors.white,
                      )
                    ],
                  )
                : Icon(
                    Icons.arrow_right_alt,
                    color: Colors.white,
                  )
          ],
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: Colors.white30,
        height: 30.0,
      ),
      itemCount: settings.length,
    );
  }
}
