import 'package:flutter/material.dart';
import 'package:once_app/ui/pages/wallet_page.dart';
import 'package:once_app/utils/constants.dart';

class WalletSetting extends StatefulWidget {
  static const tag = 'wallet_settings';
  @override
  _WalletSettingState createState() => _WalletSettingState();
}

class _WalletSettingState extends State<WalletSetting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(black),
      body: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(context, WalletPage.tag,
                        (Route<dynamic> route) => false);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  )),
              Expanded(child: Text("")),
            ],
          ),
          _settingTitle(),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(top: 5.0, left: 20.0, right: 20.0),
              child: _settingList(context),
            ),
          ),
        ],
      ),
    );
  }

  Row _settingTitle() {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(
            top: 20.0,
            bottom: 15.0,
            left: 20.0,
          ),
          child: Text(
            "General Settings",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 32,
            ),
          ),
        ),
      ],
    );
  }

  _settingList(BuildContext context) {
    final List<String> settings = <String>[
      "Payment Settings",
      "Withdrawal",
      "My Subscription",
    ];

    return ListView.separated(
      itemBuilder: (ctx, int index) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              settings[index],
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 17.0),
            ),
            Icon(
              Icons.arrow_right_alt,
              color: Colors.white,
            )
          ],
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: Colors.white30,
        height: 30.0,
      ),
      itemCount: settings.length,
    );
  }
}
