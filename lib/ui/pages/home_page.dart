import 'package:flutter/material.dart';
import 'package:flutter_arc_speed_dial/flutter_speed_dial_menu_button.dart';
import 'package:flutter_arc_speed_dial/main_menu_floating_action_button.dart';
import 'package:once_app/ui/pages/chat_page.dart';
import 'package:once_app/ui/pages/discover_page.dart';
import 'package:once_app/ui/pages/profile_page.dart';
import 'package:once_app/utils/constants.dart';
import 'package:once_app/utils/radial_menu.dart';
import 'package:switcher/core/switcher_size.dart';
import 'package:switcher/switcher.dart';

class HomePage extends StatefulWidget {
  static const tag = 'home';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  bool _isShowDial = false;
  late AnimationController _controller;

  final formKey = GlobalKey<FormState>();
  String amount = "\$75,55";
  static const List<IconData> icons = const [
    Icons.add_circle_outline,
    Icons.favorite,
    Icons.messenger,
    Icons.share_rounded
  ];

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<RadialMenuEntry> radialMenuEntries = [
      RadialMenuEntry(
          icon: Icons.mail_outline_outlined,
          text: 'Mail',
          iconColor: fromHex(white),
          // onPressed:(entry) {
          //     Navigator.of(context).pushNamed(ChatPage.tag);
          // },
      ),
      RadialMenuEntry(
          icon: Icons.search,
          text: 'Search',
          iconColor: fromHex(white),
        // onPressed:()=> _navigate(context, Discover.tag),
      ),
      RadialMenuEntry(
          icon: Icons.home_filled,
          text: 'Home',
          iconColor: fromHex(green),
        // onPressed:()=> _navigate(context, HomePage.tag),
      ),
    ];
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        width: MediaQuery.of(context).size.width / 0.14,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/bg1.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 55,
            ),
            Container(
              padding: const EdgeInsets.only(right: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Stack(
                    alignment: AlignmentDirectional.bottomCenter,
                    children: <Widget>[
                      RadialMenu(
                        entries: radialMenuEntries,
                      )
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  Positioned(
                    bottom: 30,
                    left: 30,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: new List.generate(icons.length, (int index) {
                        Widget child = new Container(
                          height: 70.0,
                          width: 50.0,
                          alignment: FractionalOffset.topCenter,
                          child: new ScaleTransition(
                            scale: new CurvedAnimation(
                              parent: _controller,
                              curve: new Interval(
                                  0.0, 1.0 - index / icons.length / 2.0,
                                  curve: Curves.easeOut),
                            ),
                            child: Column(
                              children: [
                                new FloatingActionButton(
                                  heroTag: null,
                                  backgroundColor: Colors.transparent,
                                  foregroundColor: Colors.transparent,
                                  mini: true,
                                  child: new Icon(icons[index],
                                      color: index == 1
                                          ? Colors.green
                                          : Colors.white),
                                  onPressed: () async {
                                    if (index == 0) {
                                      buyOrBid(context);
                                    }
                                    if (index == 1) {}
                                    if (index == 2) {}
                                    if (index == 3) {}
                                  },
                                ),
                                index == 0
                                    ? Container(
                                        child: Text(
                                          'Buy',
                                          style: TextStyle(
                                            color: fromHex(white),
                                          ),
                                        ),
                                      )
                                    : Container(),
                                index == 1
                                    ? Text(
                                        '4.9M',
                                        style: TextStyle(
                                          color: fromHex(white),
                                        ),
                                      )
                                    : Container(),
                                index == 2
                                    ? Text(
                                        '32.2K',
                                        style: TextStyle(
                                          color: fromHex(white),
                                        ),
                                      )
                                    : Container(),
                                index == 3
                                    ? Text(
                                        'Share',
                                        style: TextStyle(
                                          color: fromHex(white),
                                        ),
                                      )
                                    : Container(),
                                SizedBox(
                                  width: 10,
                                ),
                              ],
                            ),
                          ),
                        );
                        return child;
                      }).toList()
                        ..add(
                          FloatingActionButton(
                            heroTag: null,
                            child: CircleAvatar(
                              radius: 70,
                              backgroundImage: AssetImage(
                                "assets/fab2.png",
                              ),
                            ),
                            onPressed: () {
                              if (_controller.isDismissed) {
                                _controller.forward();
                              } else {
                                _controller.reverse();
                              }
                            },
                          ),
                        ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 30.0, left: 20.0),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      "@mikethesurfer",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      "#crypto #bitcoin #ngeni",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  SpeedDialMenuButton menus_list(BuildContext context) {
    return SpeedDialMenuButton(
      mainFABPosY: MediaQuery.of(context).size.height -
          (MediaQuery.of(context).size.height * 0.2),
      isShowSpeedDial: _isShowDial,
      updateSpeedDialStatus: (isShow) {
        this._isShowDial = isShow;
      },
      mainFABPosX: 10.0,
      isMainFABMini: false,
      mainMenuFloatingActionButton: MainMenuFloatingActionButton(
          mini: false,
          child: CircleAvatar(
            radius: 70,
            backgroundImage: AssetImage(
              "assets/fab1.png",
            ),
          ),
          onPressed: () {
            // Navigator.pushNamedAndRemoveUntil(
            //     context, ProfilePage.tag, (Route<dynamic> route) => false);
          },
          closeMenuChild: CircleAvatar(
            radius: 70,
            backgroundImage: AssetImage(
              "assets/fab1.png",
            ),
          ),
          closeMenuForegroundColor: Colors.white,
          closeMenuBackgroundColor: Colors.red),
      floatingActionButtonWidgetChildren: <FloatingActionButton>[
        FloatingActionButton(
          mini: true,
          heroTag: null,
          child: Icon(Icons.search),
          onPressed: () {
            _isShowDial = !_isShowDial;
            setState(() {});
            Navigator.pushNamedAndRemoveUntil(
                context, Discover.tag, (Route<dynamic> route) => true);
          },
          backgroundColor: Colors.orange,
        ),
        FloatingActionButton(
          mini: true,
          heroTag: null,
          child: Icon(Icons.home_filled),
          onPressed: () {
            _isShowDial = false;
            setState(() {});
          },
          backgroundColor: Colors.pink,
        ),
        FloatingActionButton(
          mini: true,
          heroTag: null,
          child: Icon(Icons.mail_outline_sharp),
          onPressed: () {
            _isShowDial = !_isShowDial;
            setState(() {});
            Navigator.pushNamedAndRemoveUntil(
                context, ChatPage.tag, (Route<dynamic> route) => true);
          },
          backgroundColor: Colors.deepPurple,
        ),
        FloatingActionButton(
          mini: true,
          heroTag: null,
          child: Icon(Icons.person_add_alt),
          onPressed: () {
            _isShowDial = !_isShowDial;
            setState(() {});
            Navigator.pushNamedAndRemoveUntil(
                context, ProfilePage.tag, (Route<dynamic> route) => true);
          },
          backgroundColor: Colors.green,
        ),
      ],
      isSpeedDialFABsMini: true,
      paddingBtwSpeedDialButton: 10.0,
    );
  }

  Future<dynamic> buyOrBid(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) {
        return Container(
          height: size.height * 0.9,
          decoration: BoxDecoration(
            color: fromHex(background),
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(50.0),
              topLeft: Radius.circular(50.0),
            ),
          ),
          // color: ,
          child: Padding(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                        child: Text(
                      "My Challenging surf video",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    )),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          CircleAvatar(
                            radius: 15,
                            backgroundImage: AssetImage(
                              "assets/fab2.png",
                            ),
                          ),
                          Text(
                            "  @mikethesurfer",
                            style: TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Current Bid:",
                      style: TextStyle(color: Colors.white),
                    ),
                    Text(
                      "Time Remaining:",
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, bottom: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "$amount",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "24h 22m 11s",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "My bid:",
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Switcher(
                            value: false,
                            colorOff: Colors.grey.withOpacity(0.3),
                            colorOn: Colors.grey,
                            onChanged: (bool state) {
                              //
                            },
                            size: SwitcherSize.small,
                          ),
                          Text(
                            ' Auto Bid',
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: Form(
                        key: formKey,
                        child: Column(
                          children: [
                            TextFormField(
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  fillColor: Colors.grey,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.green,
                                    ),
                                    borderRadius: BorderRadius.circular(25.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    borderSide: BorderSide(
                                      color: Colors.green,
                                      width: 2.0,
                                    ),
                                  ),
                                  hintText: "$amount",
                                  hintStyle: TextStyle(color: Colors.white)),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 15.0, bottom: 8.0),
                              child: SizedBox(
                                width: double.infinity, // <-- match_parent
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      primary: Colors.green),
                                  onPressed: () {},
                                  child: Text("Place a Bid"),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0, bottom: 15.0),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "OR",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ]),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, bottom: 5.0),
                              child: SizedBox(
                                width: double.infinity, // <-- match_parent
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: fromHex(background),
                                    side: BorderSide(
                                        width: 2.0, color: Colors.green),
                                  ),
                                  onPressed: () {},
                                  child: Text(
                                    "Buy now for 199,99",
                                    style: TextStyle(color: Colors.green),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  _navigate(
    BuildContext context,
    String tag
  ) {
    Navigator.of(context).pushNamed(tag);
  }
}
