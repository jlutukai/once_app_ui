import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:once_app/ui/pages/home_page.dart';
import 'package:once_app/ui/pages/settings_page.dart';
import 'package:once_app/ui/pages/take_moment.dart';
import 'package:once_app/ui/pages/wallet_page.dart';
import 'package:once_app/ui/widgets/radial_menu.dart';
import 'package:once_app/utils/constants.dart';

class ProfilePage extends StatefulWidget {
  static const tag = 'profile';
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: fromHex(black),
      floatingActionButton: FloatingActionButton(
        mini: false,
        splashColor: fromHex(primary_light),
        backgroundColor: fromHex(background),
        elevation: 2.0,
        child: Icon(Icons.add),
        shape: StadiumBorder(side: BorderSide(color: Colors.green, width: 1)),
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(
              context, TakeMoment.tag, (Route<dynamic> route) => true);
        },
      ),
      body: Column(
        children: [
          SizedBox(
            height: 50,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, HomePage.tag, (Route<dynamic> route) => true);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  )),
              Expanded(
                child: Container(
                  height: 80,
                  width: 180,
                  child: Image.asset("assets/hello.png"),
                ),
              ),
              IconButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                        context, Setting.tag, (Route<dynamic> route) => true);
                  },
                  icon: Icon(
                    Icons.settings,
                    color: Colors.white,
                  )),
            ],
          ),
          _banner(context),
          Expanded(child: _tabs(context))
        ],
      ),
    );
  }

  _banner(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30),
      width: MediaQuery.of(context).size.width,
      height: 240,
      child: Stack(
        children: [
          Positioned(
            top: 50,
            child: Container(
              color: fromHex(background),
              width: MediaQuery.of(context).size.width,
              height: 130,
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.favorite,
                                color: fromHex(white),
                                size: 16,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                "12902",
                                style: TextStyle(color: fromHex(white)),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width / 5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.person,
                                    color: fromHex(white),
                                    size: 16,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "300",
                                    style: TextStyle(color: fromHex(white)),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.person_add,
                                    color: fromHex(white),
                                    size: 16,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "100",
                                    style: TextStyle(color: fromHex(white)),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "@amybrantt",
                        style: TextStyle(color: fromHex(white), fontSize: 16),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Designer from the Wonderland.",
                        style: TextStyle(color: fromHex(white), fontSize: 16),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          // Align(
          //   alignment: Alignment.topCenter,
          //   child: Stack(
          //     alignment: AlignmentDirectional.bottomCenter,
          //     children: <Widget>[
          //       Transform(
          //         transform: Matrix4.translationValues(0, 25, 0),
          //         child: RadialMenu(),
          //       )
          //     ],
          //   ),
          // ),
        ],
      ),
    );
  }

  _tabs(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 15, right: 15),
            color: Colors.black,
            child: TabBar(
              tabs: [
                Tab(
                  text: "Vault",
                ),
                Tab(
                  text: "Owned",
                ),
                Tab(
                  text: "Liked",
                ),
              ],
              unselectedLabelColor: Colors.white,
              labelColor: fromHex(green),
              indicatorColor: Colors.transparent,
            ),
          ),
          Container(
            color: fromHex(background),
            padding: EdgeInsets.symmetric(vertical: 15),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Text(
                        "12",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                        ),
                      ),
                      Text(
                        "Minted",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Center(
                      child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(fromHex(background))),
                    child: Text(
                      "${cf.format(950)}",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(context, WalletPage.tag,
                          (Route<dynamic> route) => true);
                    },
                  )),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Text(
                        "15",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                        ),
                      ),
                      Text(
                        "Monetized",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [
                _grid(context),
                _grid(context),
                _grid(context),
              ],
            ),
          )
        ],
      ),
    );
  }

  _grid(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150.0,
        childAspectRatio: 0.5,
        crossAxisSpacing: 2.0,
        mainAxisSpacing: 5.0,
      ),
      itemBuilder: (_, index) => Padding(
        padding: const EdgeInsets.all(3.0),
        child: CachedNetworkImage(
          imageUrl: image,
          fit: BoxFit.cover,
        ),
      ),
      itemCount: 100,
    );
  }
}
