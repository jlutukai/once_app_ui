import 'package:flutter/material.dart';
import 'package:once_app/ui/pages/chat_page.dart';
import 'package:once_app/ui/pages/discover_page.dart';
import 'package:once_app/ui/pages/home_page.dart';
import 'package:once_app/ui/pages/profile_page.dart';
import 'package:once_app/utils/constants.dart';
import 'dart:math';
import 'package:vector_math/vector_math.dart' show radians;

// class RadialMenu extends StatefulWidget {
//   createState() => _RadialMenuState();
// }
//
// class _RadialMenuState extends State<RadialMenu>
//     with SingleTickerProviderStateMixin {
//   late AnimationController controller;
//   @override
//   void initState() {
//     super.initState();
//     controller =
//         AnimationController(duration: Duration(milliseconds: 200), vsync: this);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return RadialAnimation(controller: controller);
//   }
// }
//
// // The Animation
// class RadialAnimation extends StatelessWidget {
//   RadialAnimation({Key? key, required this.controller})
//       : scale = Tween<double>(
//           begin: 1.5,
//           end: 0.0,
//         ).animate(
//           CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn),
//         ),
//         translation = Tween<double>(
//           begin: 0.0,
//           end: 45.0,
//         ).animate(
//           CurvedAnimation(parent: controller, curve: Curves.bounceOut),
//         ),
//         opacity = Tween<double>(
//           begin: 0.0,
//           end: 1.0,
//         ).animate(
//           CurvedAnimation(parent: controller, curve: Curves.easeIn),
//         ),
//         rotation = Tween<double>(
//           begin: 0.0,
//           end: 360.0,
//         ).animate(
//           CurvedAnimation(
//             parent: controller,
//             curve: Interval(
//               0.3,
//               0.9,
//               curve: Curves.decelerate,
//             ),
//           ),
//         ),
//         super(key: key);
//   final AnimationController controller;
//   final Animation<double> scale;
//   final Animation<double> opacity;
//   final Animation<double> translation;
//   final Animation<double> rotation;
//   @override
//   Widget build(context) {
//     return AnimatedBuilder(
//         animation: controller,
//         builder: (context, builder) {
//           return Stack(alignment: Alignment.center, children: [
//             FloatingActionButton(
//               heroTag: null,
//               onPressed: () {
//                 Navigator.pushNamedAndRemoveUntil(
//                     context, ChatPage.tag, (Route<dynamic> route) => true);
//                 _toggle();
//                 print("MESSAGE");
//               },
//               child: _buildButton(
//                 87,
//                 color: Colors.white,
//                 icon: Icons.mail_outline,
//                 spaceCenterRatio: 1.6,
//               ),
//             ),
//             GestureDetector(
//               onTap: () {
//                 Navigator.pushNamedAndRemoveUntil(
//                     context, Discover.tag, (Route<dynamic> route) => true);
//                 _toggle();
//                 print("Search");
//               },
//               child: _buildButton(
//                 150,
//                 color: Colors.white,
//                 icon: Icons.search,
//                 spaceCenterRatio: 1.6,
//               ),
//             ),
//             FloatingActionButton(
//               heroTag: null,
//               onPressed: () {
//                 Navigator.pushNamedAndRemoveUntil(
//                     context, HomePage.tag, (Route<dynamic> route) => false);
//                 _toggle();
//
//                 print("HOME");
//               },
//               child: _buildButton(
//                 210,
//                 color: Colors.greenAccent,
//                 icon: Icons.home,
//                 spaceCenterRatio: 1.6,
//               ),
//             ),
//             Transform.scale(
//               scale: 1,
//               child: Container(
//                 width: 65,
//                 height: 65,
//                 child: FittedBox(
//                   child: FloatingActionButton(
//                     heroTag: null,
//                       elevation: 10,
//                       backgroundColor: Colors.transparent,
//                       child: InkWell(
//                         child: CircleAvatar(
//                           radius: 52,
//                           backgroundImage: AssetImage(
//                             "assets/fab1.png",
//                           ),
//                         ),
//                         onTap: () {
//                           _toggle();
//                           print("MENU");
//                         },
//                         onDoubleTap: () {
//                           Navigator.pushNamedAndRemoveUntil(context,
//                               ProfilePage.tag, (Route<dynamic> route) => true);
//                           _toggle();
//                         },
//                       ),
//                       onPressed: () {}),
//                 ),
//               ),
//             )
//           ]);
//         });
//   }
//
//   _buildButton(double angle,
//       {required Color color, required IconData icon, spaceCenterRatio = 1}) {
//     final double rad = radians(angle);
//     return Transform(
//         transform: Matrix4.identity()
//           ..translate((translation.value) * cos(rad) * spaceCenterRatio,
//               (translation.value) * sin(rad) * spaceCenterRatio),
//         child: Opacity(
//           opacity: opacity.value,
//           child: Container(
//             width: 40,
//             height: 40,
//             decoration: BoxDecoration(
//                 color: fromHex(background),
//                 borderRadius: BorderRadius.circular(50)),
//             child: Icon(
//               icon,
//               size: 26,
//               color: color,
//             ),
//           ),
//         ));
//   }
//
//   _toggle() {
//     if (controller.value == 0) {
//       controller.forward();
//     }
//     if (controller.value == 1) {
//       controller.reverse();
//     }
//   }
// }
