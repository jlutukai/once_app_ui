import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

Widget loader() {
  return Center(
    child: CircularProgressIndicator(
      backgroundColor: fromHex(maroon),
      valueColor: new AlwaysStoppedAnimation<Color>(
        fromHex(dark_purple),
      ),
    ),
  );
}

var df = DateFormat("dd/MM/yyyy");
var tf = DateFormat("HH:mm");

var cf = NumberFormat.currency(locale: "en_US", symbol: '\$', decimalDigits: 0);

const String image =
    "https://images.unsplash.com/photo-1630560264774-167d0ae1180b?ixid=MnwxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwxNTJ8fHxlbnwwfHx8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60";
const String green = "#28E98C";
const String some_color = '#707070';
const String dark_purple = "#800080";
const String maroon = "#630330";
const String light_maroon = "#721C44";
const String lite_yellow = "#F9E4B0";
const String dark_green = "#008000";
const String blue = "#033063";
const String light_blue = "#9BA5FF";
const String lite_blue = "#D8DCFF";
const String grey = '#707070';
const String red = '#FF0000';
const String background = "#1D1C1F";
const String black = "#000000";
const String white = "#ffffff";
const String primary_light = "#3D3C41";
const String lorem =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem unknown printer took a galley of type and scrambled it to make a type specimen book.";

showToast(String msg) {
  Fluttertoast.showToast(msg: msg);
}

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
}

getBorder() {
  return UnderlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: Colors.white),
  );
}

getEnabledBorder() {
  return UnderlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: fromHex(dark_purple), width: 3),
  );
}

setToken(String s) {
  // Box<String> accountInfo = Hive.box<String>("account_info");
  // accountInfo.put("token", s);
}

String getToken() {
  // Box<String> accountInfo = Hive.box<String>("account_info");
  return "";
}

deleteToken() {
  // Box<String> accountInfo = Hive.box<String>("account_info");
  // return accountInfo.delete("token");
}

// setAddUserData(CreateAccountBody s) {
//   Box user = Hive.box("create_user");
//   user.put("user", s);
// }
//
// CreateAccountBody getAddUserData() {
//   Box user = Hive.box("create_user");
//   return user.get("user");
// }

deleteAddUserData() {}

deleteEveryThing() {
  deleteAddUserData();
  deleteToken();
}

// TO BE REMOVED CONTENT
const List rooms = [
  {
    "name": "Work",
    "imageUrl": "",
  },
  {
    "name": "Ngeni.io",
    "imageUrl": "",
  },
  {
    "name": "In Kenya",
    "imageUrl": "",
  },
  {
    "name": "USA ",
    "imageUrl": "",
  },
];

const List conversationList = [
  {
    "name": "Dave Mitchel",
    "message": "Dude, are you ready for some great news about this weekend?",
    "time": "7:00 am",
    "unread": 2,
    "isOnline": true
  },
  {
    "name": "Bettany Rogers",
    "message": "Dude, are you ready for some great news about this weekend?",
    "time": "6:50 am",
    "isOnline": true
  },
  {
    "name": "Britney Huddson",
    "message": "Dude, are you ready for some great news about this weekend?",
    "time": "2nd Feb",
    "unread": 10,
    "isOnline": false
  },
  {
    "name": "Thomas Berringer",
    "message": "Dude, are you ready for some great news about this weekend?",
    "time": "28th Jan",
    "unread": 0,
    "isOnline": false
  },
  {
    "name": "Britney Huddson",
    "message": "Dude, are you ready for some great news about this weekend?",
    "time": "25th Jan",
    "unread": 2,
    "isOnline": false
  },
  {
    "name": "Bettany Rogers",
    "message": "Dude, are you ready for some great news about this weekend?",
    "time": "15th Jan",
    "unread": 0,
    "isOnline": false
  }
];

const List<dynamic> transactionsSummary = [
  {
    "title": "The Great Inspiration.png",
    "type": "PURCHASE",
    "color": white,
    "label": 0xffffff,
    "price": "-19.00"
  },
  {
    "title": "The Photo of the wall",
    "type": "SALES",
    "color": green,
    "label": 0x28E98C,
    "price": "99.99"
  },
  {
    "title": "The Great Inspiration.png",
    "type": "DECLINED",
    "color": red,
    "label": 0xFF0000,
    "price": "-19.00"
  },
  {
    "title": "My account top up",
    "type": "TOP UP",
    "color": green,
    "label": 0x28E98C,
    "price": "5.00"
  },
  {
    "title": "Withdraw to **5671",
    "type": "PURCHASE",
    "color": white,
    "label": 0xffffff,
    "price": "-1,500.00"
  },
];

Color getTransctionSummyBgColor(String type) {
  if (type == "PURCHASE") {
    return Colors.white;
  } else if (type == "TOP UP") {
    return Colors.green;
  } else if (type == "DECLINED") {
    return Colors.red.shade900;
  } else if (type == "SALES") {
    return Colors.green;
  }
  return Colors.red.shade900;
}
