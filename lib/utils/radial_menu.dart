import 'dart:math';

import 'package:flutter/material.dart';
import 'package:once_app/ui/pages/chat_page.dart';
import 'package:once_app/ui/pages/discover_page.dart';
import 'package:once_app/ui/pages/home_page.dart';
import 'package:once_app/ui/pages/profile_page.dart';

class RadialMenu extends StatefulWidget {
  final List<RadialMenuEntry> entries;
  final Color color;
  final double size;
  final double entrySize;

  const RadialMenu(
      {required this.entries,
        this.color = Colors.orangeAccent,
        this.size = 160,
        this.entrySize = 85});

  @override
  State<StatefulWidget> createState() => _RadialMenuState();
}

/// The [RadialMenuEntry] with icons, callbacks, text and colors
class RadialMenuEntry {
  /// The callback to trigger on an [RadialMenuEntry] tap
  final Function? onPressed;

  /// The[RadialMenuEntry icon
  final IconData icon;

  /// The [RadialMenuEntry] icon color
  Color? iconColor;

  /// The [RadialMenuEntry] color. Defaults to [Colors.black].
  final Color color;

  /// The [RadialMenuEntry] text shown above the icon. Defaults to "".
  final String text;

  /// The [RadialMenuEntry] text color. Defaults to [Colors.black].
  Color? textColor;

  /// The [RadialMenuEntry] icon size. Defaults to 24.
  final double iconSize;

  RadialMenuEntry(
      { this.onPressed,
        required this.icon,
        this.color = Colors.black,
        this.text = '',
        iconColor,
        textColor,
        this.iconSize = 24}) {
    this.textColor = textColor ?? color;
    this.iconColor = iconColor ?? color;
  }
}

class _RadialMenuState extends State<RadialMenu> {
  bool open = false;

  @override
  Widget build(BuildContext context) {
    Size subCategorySize = Size(widget.entrySize, widget.entrySize);
    Size mainSize = Size(widget.size, widget.size);

    return Container(
      width: mainSize.width,
      height: mainSize.height,
      decoration: BoxDecoration(
          color:
          open == true ? Colors.transparent : Colors.transparent,
          borderRadius: BorderRadius.all(Radius.circular(mainSize.width))),
      child: Stack(children: <Widget>[
        Align(
            alignment: Alignment.center,
            child: GestureDetector(
              onDoubleTap: (){
                // if(open){
                  Navigator.of(context).pushNamed(ProfilePage.tag);
                // }
              },
              child: FloatingActionButton(
                  heroTag: 'togglePartners',
                  mini: false,
                  child: CircleAvatar(
                    radius: 70,
                    backgroundImage: AssetImage(
                      "assets/fab1.png",
                    ),
                  ),
                  backgroundColor: widget.color,
                  elevation: 0,
                  onPressed: () {

                    setState(() {
                      open = !open;
                    });
                  }),
            )),
        if (open)
          ..._renderWheel(
            widget.entries,
            context,
            parentSize: mainSize,
            childSize: subCategorySize,

          ),
      ]),
    );
  }
}

class _CenterRotated extends StatelessWidget {
  final Size parentSize;
  final Size size;
  final double angle;
  final Widget child;

  _CenterRotated(
      {this.angle = 0,
        required this.size,
        required this.parentSize,
        required this.child});

  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
        angle: angle+pi,
        origin: Offset(parentSize.width / 2 - 50/ 2,
            parentSize.height / 2 - 50 / 2),
        child: Container(
          width: 50,
          height: 50,
          child: Transform.rotate(angle: -angle+pi, child: child),
        ));
  }
}

List<Widget> _renderWheel(List<RadialMenuEntry> entries, BuildContext context,
    {required Size parentSize, required Size childSize,  Function? onPressed}) {
  return entries
      .asMap()
      .map((index, entry) => MapEntry(
      index,
      _CenterRotated(
        angle: pi / 4 + index * pi / (entries.length + 1),
        size: childSize,
        parentSize: parentSize,
        child: FloatingActionButton(onPressed: (){
          switch(index){
            case 0:
              Navigator.of(context).pushNamed(ChatPage.tag);
              break;
            case 1:
              Navigator.of(context).pushNamed(Discover.tag);
              break;
            case 2:
              Navigator.of(context).pushNamed(HomePage.tag);
              break;
          }
          // onPressed is Function
          //     ? onPressed(entry)
          //     : print('entry $index tapped without onTap callback');
        },
        heroTag: null,
          mini: true,
          backgroundColor: Colors.grey,
        child:  Icon(
          entry.icon,
          color: entry.iconColor,
          size: entry.iconSize,
        ),
        ),
      )))
      .values
      .toList();
}