import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:once_app/core/models/create_account_response.dart';

class Api {
  static const baseUrl =
      'https://authentication-service-wrbj57luca-uc.a.run.app';
  late Dio _dio;
  String firebaseId = "";

  Api() {
    BaseOptions options =
        BaseOptions(receiveTimeout: 50000, connectTimeout: 50000);
    _dio = Dio(options)
      ..interceptors.add(LogInterceptor(requestBody: true, responseBody: true))
      ..interceptors
          .add(DioCacheManager(CacheConfig(baseUrl: baseUrl)).interceptor)
      ..options.headers["content-type"] = "application/json";
  }

  Future<CreateAccountResponse> registerUser(Map<String, String> data) async {
    String endPoint = "$baseUrl/api/v1/register";
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return CreateAccountResponse.fromJson(response.data);
  }

  Future<CreateAccountResponse> loginUser(Map<String, String> data) async {
    String endPoint = "$baseUrl/api/v1/login";
    final response = await _dio.post(endPoint, data: data).catchError((e) {
      throw e.response.toString();
    });
    return CreateAccountResponse.fromJson(response.data);
  }
}

//https://www.getpostman.com/collections/095b8f70005975c81589
