class CreateAccountResponse {
  bool? success;
  String? token;
  // AccountData? createAccountData;

  CreateAccountResponse({
    this.success,
    this.token,
    // this.createAccountData,
  });
  CreateAccountResponse.fromJson(Map<String, dynamic> json) {
    success = json["success"];
    token = json["token"]?.toString();
    // createAccountData =
    //     (json["data"] != null) ? AccountData.fromJson(json["data"]) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["success"] = success;
    data["token"] = token;
    // if (createAccountData != null) {
    //   data["data"] = this.createAccountData!.toJson();
    // }
    return data;
  }
}
