import 'package:get_it/get_it.dart';

import 'core/services/api.dart';
import 'core/viewmodels/login_model.dart';

GetIt locator = GetIt.instance;

void setUpLocator() {
  locator.registerLazySingleton(() => Api());

  locator.registerFactory(() => LoginModel());
}
