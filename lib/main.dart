import 'package:flutter/material.dart';
import 'package:once_app/ui/pages/chat_page.dart';
import 'package:once_app/ui/pages/discover_page.dart';
import 'package:once_app/ui/pages/home_page.dart';
import 'package:once_app/ui/pages/profile_page.dart';
import 'package:once_app/ui/pages/question_page.dart';
import 'package:once_app/ui/pages/settings_page.dart';
import 'package:once_app/ui/pages/single_chat_page.dart';
import 'package:once_app/ui/pages/splash_page.dart';
import 'package:once_app/ui/pages/take_moment.dart';
import 'package:once_app/ui/pages/wallet_page.dart';
import 'package:once_app/ui/pages/wallet_settings_page.dart';

// https://fireship.io/lessons/flutter-radial-menu-staggered-animations/
void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Once App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      routes: {
        '/': (context) => SplashScreen(),
        HomePage.tag: (context) => HomePage(),
        ProfilePage.tag: (context) => ProfilePage(),
        Discover.tag: (context) => Discover(),
        Setting.tag: (context) => Setting(),
        ChatPage.tag: (context) => ChatPage(),
        TakeMoment.tag: (context) => TakeMoment(),
        WalletPage.tag: (context) => WalletPage(),
        WalletSetting.tag: (context) => WalletSetting(),
        QuestionPage.tag: (context) => QuestionPage(),
        SingleChatPage.tag: (context) => SingleChatPage()
      },
    );
  }
}
